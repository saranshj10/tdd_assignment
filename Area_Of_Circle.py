from Resources import input_file

class Area():
    def findArea(self, r):
        PI = 3.142
        return round(PI * (r * r))


radius = input_file.myradius
if radius < 0:
    raise ValueError("Radius Can't be Negative")
circlearea = Area()
area_circle = circlearea.findArea(radius)
print(area_circle)