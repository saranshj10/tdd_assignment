import unittest
import Area_Of_Circle
myarea = Area_Of_Circle.Area()
class Testfind_area(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("Executing Class")

    def setUp(self):
        print("Executing test cases")

    def test_findArea(self):
        self.assertEqual(myarea.findArea(7),154)
        self.assertEqual(myarea.findArea(1), 3)
        self.assertEqual(myarea.findArea(10), 314)
        self.assertEqual(myarea.findArea(0), 0)
        self.assertEqual(myarea.findArea(-2), ValueError)
        self.assertRaises(ValueError)

    def tearDown(self):
        print("Stopping the running test cases")

    @classmethod
    def tearDownClass(cls):
        print("Program executed succesfully")


        
if __name__ == '__main__':
    unittest.main()
